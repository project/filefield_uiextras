
-- SUMMARY --

A helper module that provides a collection of miscellaneous FileField UI enhancements.

1) Collapsible fieldset wrapper on the data elements.

These include the alt, title, description fields provided by FileField and any additional
fields that are created by other modules, name ImageField Extended.

Possible options (per field) are:
- Skip this field
- Always collapsed
- Always open
- Conditionally collapsed when data is entered
- Conditionally collapsed when no data is entered
- Conditionally collapsed on new uploads only
- Conditionally open on new uploads only

-- REQUIREMENTS --

* CCK
* FileField

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Configure the settings on the CCK field instance settings page.
  
-- CONTACT --

Current maintainers:
* Alan Davison (Alan D.) - http://www.caignwebs.com.au

